package model;
import presentation.*;


public class Product {
	private String name;
	private int price;
	private int stock;
	private int idProduct;
	
	public Product(String name, int price, int stock,int idProduct){
		this.setName(name);
		this.setPrice(price);
		this.stock=-stock;
		this.setIdProduct(idProduct);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	

	public int getStock() {
		return stock;
	}

	public void setStock(int quantity) {
		this.stock -=quantity;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	@Override
	public String toString() {
		return "Product [idProduct=" + idProduct + ", name=" + name + ", price=" + price + ", stock=" + stock + "]";
	}
	
}
