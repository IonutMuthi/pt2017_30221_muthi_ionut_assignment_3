package model;
import presentation.*;


import java.io.PrintWriter;
import java.util.ArrayList;

public class Order {
	private int idOrder;
	private int pretTotal=0;
	private int clientId;
	private int productId;
	public static String bon="";
	private ArrayList<Product> order=new ArrayList();
	
	public Order(){
		
		
	}
	
	public void addProduct(Product p,int quantity){
		
		if(quantity<=(-p.getStock())){
		order.add(p);
		pretTotal+=p.getPrice()*quantity;
		p.setStock(quantity);
		bon+=""+p.getName()+"........."+p.getPrice()+" lei x"+quantity+"\r\n";
		}
		else
			System.out.println("\nNot enough "+p.getName()+" left \n We only have "+p.getStock());
	}
	
	public int getPretTotal(){
		
		return pretTotal;
	}
	
	public String getBon(){
		
		bon+="Pret total=  "+getPretTotal()+" lei";
	
		try {
		    PrintWriter writer = new PrintWriter("Bon.txt", "UTF-8");
		    writer.println(bon);
		    writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return bon;
	}
	

	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}

	

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}
}
