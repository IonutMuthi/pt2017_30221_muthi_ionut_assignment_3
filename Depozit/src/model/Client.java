package model;


import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Client {
	private int idClient;
	private String name;
	private String address;
	private String email;
	Order o=new Order();
	public ArrayList<Order> orders=new ArrayList();
	
	public Client(int idClient,String name,String address,String email ){
		this.idClient=idClient;
		this.setName(name);
		this.setAddress(address);
		this.setEmail(email);
		
	}
	
	public void makeOrder(){
		o=new Order();
		orders.add(o);
		o.setClientId(idClient);
		o.setIdOrder(orders.indexOf(o));
	}
	
	public void addToOrder(Product p,int quantity){
		
		o.addProduct(p, quantity);
		o.setProductId(p.getIdProduct());
		System.out.println(" order complete");
		
	}
	public void finnishOrder(){
		o.getBon();
		System.out.println("Bon printat ");
	//	System.out.println(o.getBon());
	}
	
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return "Client [idClient=" + idClient + ", name=" + name + ", email=" + email + ", address=" + address + "]";
	}
	
}
