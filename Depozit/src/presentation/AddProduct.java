package presentation;
import model.*;
import connection.*;
import bll.*;
import dao.*;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;



public class AddProduct extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtPrice;
	private JTextField txtStock;
	private JLabel lblIntroduceTheClient;
	public String name;
	public int price;
	public int stock;
	public int idProduct;
	private JLabel lblProductAddedSuccesfully;
	ConnectionFactory conFact= new ConnectionFactory();
	ProductDAO daoP=new ProductDAO();
	ProductBLL bllP=new ProductBLL();
	private JTextField txtIdProduct;
	private JLabel lblIdproduct;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddProduct frame = new AddProduct();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddProduct() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 300, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(38, 41, 64, 14);
		contentPane.add(lblName);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(38, 66, 46, 14);
		contentPane.add(lblPrice);
		
		JLabel lblStock = new JLabel("Stock");
		lblStock.setBounds(38, 91, 52, 14);
		contentPane.add(lblStock);
		
		txtName = new JTextField();
		txtName.setBounds(97, 38, 86, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		txtPrice = new JTextField();
		txtPrice.setBounds(97, 63, 86, 20);
		contentPane.add(txtPrice);
		txtPrice.setColumns(10);
		
		txtStock = new JTextField();
		txtStock.setBounds(97, 88, 86, 20);
		contentPane.add(txtStock);
		txtStock.setColumns(10);
		
		lblIntroduceTheClient = new JLabel("Introduce the product data");
		lblIntroduceTheClient.setBounds(48, 11, 163, 19);
		contentPane.add(lblIntroduceTheClient);
		
		lblProductAddedSuccesfully = new JLabel("Product added succesfully");
		lblProductAddedSuccesfully.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblProductAddedSuccesfully.setBounds(48, 165, 185, 48);
		lblProductAddedSuccesfully.setVisible(false);
		contentPane.add(lblProductAddedSuccesfully);
		
		JButton btnAddProduct = new JButton("AddProduct");
		btnAddProduct.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				name=txtName.getText();
				price=Integer.parseInt(txtPrice.getText());
				stock=-Integer.parseInt(txtStock.getText());
				idProduct=Integer.parseInt(txtIdProduct.getText());
				btnAddProduct.setVisible(false);
				lblProductAddedSuccesfully.setVisible(true);
				try {
					Product p=new Product(name,price,stock,idProduct);
					bllP.insertProduct(p);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnAddProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAddProduct.setBounds(81, 180, 102, 23);
		contentPane.add(btnAddProduct);
		
		txtIdProduct = new JTextField();
		txtIdProduct.setBounds(97, 119, 86, 20);
		contentPane.add(txtIdProduct);
		txtIdProduct.setColumns(10);
		
		lblIdproduct = new JLabel("idProduct");
		lblIdproduct.setBounds(25, 122, 59, 14);
		contentPane.add(lblIdproduct);
		
		
	}

}
