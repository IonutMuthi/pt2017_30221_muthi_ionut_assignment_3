package presentation;
import model.*;
import connection.*;
import bll.*;
import dao.*;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;



import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class ShowClients extends JFrame {

	private JPanel contentPane;
	private JTable table;
	ConnectionFactory conFact= new ConnectionFactory();
	ClientBLL bllC=new ClientBLL();
	private JTextField textField;
	private JLabel lblInputTheClient;
	private JButton btnUpdateClient;
	ArrayList<Client> clients=bllC.getClients();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowClients frame = new ShowClients();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public ShowClients() throws SQLException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		

		String[] cols={"idClient","Name","Address","Email"};
		
		DefaultTableModel model=new DefaultTableModel();
		model.addColumn("idClient");
		model.addColumn("Name");
		model.addColumn("Address");
		model.addColumn("Email");
		model.addRow(cols);
		
		for(Client s:clients){
			Object[] o={s.getIdClient(),s.getName(),s.getAddress(),s.getEmail()};
			model.addRow(o);
			
		}
    	table = new JTable(model);
		table.setBounds(10, 11, 414, 189);
		contentPane.add(table);

		textField = new JTextField();
		textField.setBounds(251, 211, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnDeleteSelectedClient = new JButton("Delete Client");
		btnDeleteSelectedClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDeleteSelectedClient.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int id=Integer.parseInt(textField.getText());
				try {
					bllC.deleteClient(id);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnDeleteSelectedClient.setBounds(310, 261, 153, 23);
		contentPane.add(btnDeleteSelectedClient);
		
		
		lblInputTheClient = new JLabel("Input the client id");
		lblInputTheClient.setBounds(116, 211, 106, 23);
		contentPane.add(lblInputTheClient);
		
		btnUpdateClient = new JButton("Update Client");
		btnUpdateClient.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int id=Integer.parseInt(textField.getText());
				//int id1=Integer.parseInt((String) model.getValueAt(id, 0));
				String name=(String) model.getValueAt(id+1, 1);
				String address=(String) model.getValueAt(id+1, 2);
				String email = (String) model.getValueAt(id+1, 3);
				Client c=new Client(id,name,address,email);
				try {
					bllC.updateClient(c, id);
					System.out.println("update done ");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnUpdateClient.setBounds(162, 261, 120, 23);
		contentPane.add(btnUpdateClient);
		
		JButton btnAddNewClient = new JButton("Add new client");
		btnAddNewClient.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				AddClient addC=new AddClient();
				addC.setVisible(true);
			}
		});
		
		btnAddNewClient.setBounds(10, 261, 131, 23);
		contentPane.add(btnAddNewClient);
	}
}
