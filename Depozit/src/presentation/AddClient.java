package presentation;
import model.*;
import connection.*;
import bll.*;
import dao.*;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.awt.Font;

public class AddClient extends JFrame {

	private JPanel contentPane;
	private JTextField txtIdClient;
	private JTextField txtName;
	private JTextField txtAddress;
	private JTextField txtEmail;
	private JLabel lblIntroduceTheClient;
	public int idClient;
	public String name;
	public String address;
	public String email;
	private JLabel lblClientAddedSuccesfully;
	ConnectionFactory conFact= new ConnectionFactory();
	ClientDAO daoC=new ClientDAO();
	ClientBLL bllC=new ClientBLL();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddClient frame = new AddClient();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddClient() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 300, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIdclient = new JLabel("idClient");
		lblIdclient.setBounds(38, 41, 64, 14);
		contentPane.add(lblIdclient);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(38, 66, 46, 14);
		contentPane.add(lblName);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(38, 91, 52, 14);
		contentPane.add(lblAddress);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(38, 116, 46, 14);
		contentPane.add(lblEmail);
		
		txtIdClient = new JTextField();
		txtIdClient.setBounds(97, 38, 86, 20);
		contentPane.add(txtIdClient);
		txtIdClient.setColumns(10);
		
		txtName = new JTextField();
		txtName.setBounds(97, 63, 86, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		txtAddress = new JTextField();
		txtAddress.setBounds(97, 88, 86, 20);
		contentPane.add(txtAddress);
		txtAddress.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(97, 113, 86, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		lblIntroduceTheClient = new JLabel("Introduce the client data");
		lblIntroduceTheClient.setBounds(48, 11, 163, 19);
		contentPane.add(lblIntroduceTheClient);
		
		lblClientAddedSuccesfully = new JLabel("Client added succesfully");
		lblClientAddedSuccesfully.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblClientAddedSuccesfully.setBounds(48, 165, 185, 48);
		lblClientAddedSuccesfully.setVisible(false);
		contentPane.add(lblClientAddedSuccesfully);
		
		JButton btnAddclient = new JButton("AddClient");
		btnAddclient.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				idClient=Integer.parseInt(txtIdClient.getText());
				name=txtName.getText();
				address=txtAddress.getText();
				email=txtEmail.getText();
				btnAddclient.setVisible(false);
				lblClientAddedSuccesfully.setVisible(true);
				try {
					bllC.insertClient(new Client(idClient,name,address,email));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnAddclient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAddclient.setBounds(81, 180, 102, 23);
		contentPane.add(btnAddclient);
		
		
	}
	
	public int getIdClient() {
		return idClient;
	}
	
	
	public String getName() {
		
		return name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getEmail() {
		return email;
	}
	
}
