package presentation;
import model.*;
import connection.*;
import bll.*;
import dao.*;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import model.*;
import connection.*;

public class ShowProducts extends JFrame {

	private JPanel contentPane;
	private JTable table;
	ConnectionFactory conFact= new ConnectionFactory();
	ProductBLL bllP=new ProductBLL();
	private JButton btnUpdateProduct;
	private JTextField textField;
	private JLabel lblInputTheProduct;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowProducts frame = new ShowProducts();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public ShowProducts() throws SQLException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 380);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		String[] cols={"idProduct","Name","Price","Stock"};
		
		DefaultTableModel model=new DefaultTableModel();
		model.addColumn("idProduct");
		model.addColumn("Name");
		model.addColumn("Price");
		model.addColumn("Stock");
		model.addRow(cols);
		
		for(Product p:bllP.getProducts()){
			Object[] o={p.getIdProduct(),p.getName(),p.getPrice(),p.getStock()};
			model.addRow(o);
			
		}
		contentPane.setLayout(null);
		table = new JTable(model);
		table.setBounds(10, 11, 414, 189);
		contentPane.add(table);
		
		textField = new JTextField();
		textField.setBounds(232, 212, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnDeleteSelectedProduct = new JButton("Delete product");
		btnDeleteSelectedProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDeleteSelectedProduct.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				int id =Integer.parseInt(textField.getText());
				try {
					bllP.deleteProduct(id);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnDeleteSelectedProduct.setBounds(321, 254, 153, 36);
		contentPane.add(btnDeleteSelectedProduct);
		
		
		lblInputTheProduct = new JLabel("Input the product id");
		lblInputTheProduct.setBounds(88, 211, 136, 23);
		contentPane.add(lblInputTheProduct);
		
		btnUpdateProduct = new JButton("Update product");
		btnUpdateProduct.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				try {
					int id=Integer.parseInt(textField.getText());
					String name=(String) model.getValueAt(id+1, 1);
					int price=(Integer) model.getValueAt(id+1, 2);
					int stock =(Integer) model.getValueAt(id+1, 3);
					Product p=new Product(name,price,stock,id);
					bllP.updateProduct(p, id);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println("update done ");
				
				
			}
		});
		btnUpdateProduct.setBounds(165,254, 135, 36);
		contentPane.add(btnUpdateProduct);
		
		JButton btnAddNewProduct = new JButton("Add new product");
		btnAddNewProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAddNewProduct.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				AddProduct addP=new AddProduct();
				addP.setVisible(true);
			}
		});
		
		btnAddNewProduct.setBounds(10, 254, 145, 36);
		contentPane.add(btnAddNewProduct);
	}

}
