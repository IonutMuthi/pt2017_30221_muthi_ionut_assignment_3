package presentation;
import model.*;
import connection.*;
import bll.*;
import dao.*;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MakeOrder extends JFrame {

	private JPanel contentPane;
	private JTextField txtIdClient;
	private JTextField txtProductId;
	private JTextField txtQuantity;
	ConnectionFactory conFact= new ConnectionFactory();
	OrderBLL bllO=new OrderBLL();
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MakeOrder frame = new MakeOrder();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public MakeOrder() throws SQLException {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 270, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		 ArrayList<Client> clients=ClientDAO.getClients();
		 ArrayList<Product> products=ProductDAO.getProducts();
		
		JLabel lblClientId = new JLabel("Client id");
		lblClientId.setBounds(30, 32, 46, 14);
		contentPane.add(lblClientId);
		
		JLabel lblProductId = new JLabel("Product id");
		lblProductId.setBounds(30, 57, 94, 14);
		contentPane.add(lblProductId);
		
		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(30, 82, 61, 14);
		contentPane.add(lblQuantity);
		
		JButton btnAddToOrder = new JButton("Add to order");
		btnAddToOrder.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			
				try {
					bllO.addProduct(Integer.parseInt(txtProductId.getText()), Integer.parseInt(txtQuantity.getText()));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnAddToOrder.setBounds(64, 120, 125, 34);
		contentPane.add(btnAddToOrder);
		
		JButton btnFinnishOrder = new JButton("Finnish order");
		btnFinnishOrder.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			bllO.finnishOrder();
			}
		});
		
		btnFinnishOrder.setBounds(64, 165, 125, 34);
		contentPane.add(btnFinnishOrder);
		

		JButton btnNewOrder = new JButton("New Order");
		 btnNewOrder.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			try {
					System.out.println("Order created");
					bllO.makeOrder(Integer.parseInt(txtIdClient.getText()));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		btnNewOrder.setBounds(64, 210, 125, 34);
		contentPane.add(btnNewOrder);
		
		txtIdClient = new JTextField();
		txtIdClient.setBounds(156, 29, 86, 20);
		contentPane.add(txtIdClient);
		txtIdClient.setColumns(10);
		
		txtProductId = new JTextField();
		txtProductId.setBounds(156, 54, 86, 20);
		contentPane.add(txtProductId);
		txtProductId.setColumns(10);
		
		txtQuantity = new JTextField();
		txtQuantity.setBounds(156, 79, 86, 20);
		contentPane.add(txtQuantity);
		txtQuantity.setColumns(10);
	}
}
