package bll;
import java.awt.List;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;



public class TableHead {
	
	public static JTable createTable(ArrayList<Object> object) {

		if(object.isEmpty()){
			return new JTable();
		}
		DefaultTableModel model=new DefaultTableModel();
		for (Field field : object.get(0).getClass().getDeclaredFields()) {
			model.addColumn(field.getName()); 
		}
			for(Object obj:object){
				Vector<Object>row=new Vector<Object>();
				for (Field field : object.get(0).getClass().getDeclaredFields()) {
					field.setAccessible(true);
					try {
						row.add(field.get(obj));
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					
			}
				model.addRow(row);
		}
			JTable table =new JTable(model);
			return table;
	}
	
	

}
