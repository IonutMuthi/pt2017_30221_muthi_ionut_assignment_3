package bll;
import model.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import bll.validators.*;
import dao.*;


public class ProductBLL {
	IdProductValidator idValidator;
	
	public ProductBLL(){
	//	 idValidator=new IdProductValidator();
	
	}
	public Product findProductById(int id){
		Product p=ProductDAO.findById(id);
		if(p==null){
			throw new NoSuchElementException("The Product with id =" + id + " was not found!");
			
		}
		return p;
	}
	
	public int insertProduct(Product Product) throws SQLException{
	//	idValidator.validate(Product);
		return ProductDAO.addProduct(Product);
	}
	
	public ArrayList<Product> getProducts() throws SQLException{
		return ProductDAO.getProducts();
	}
	
	public void deleteProduct(int id) throws SQLException{
		ProductDAO.deleteProduct(id);
	}
	
	public void updateProduct(Product p, int id) throws SQLException{
		ProductDAO.updateProduct(p, id);
	}
}
