package bll.validators;
import model.*;
import dao.*;

public class IdClientValidator implements Validator<Client> {
		ClientDAO daoC=new ClientDAO();
	@Override
	public void validate(Client c) {
		Client c1=daoC.findById(c.getIdClient());
		if(c1!=null){
			throw new IllegalArgumentException("A client with that id already exists");
		}
		
		
	}

	

}
