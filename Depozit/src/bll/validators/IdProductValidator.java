package bll.validators;
import model.*;
import dao.*;

public class IdProductValidator implements Validator<Product> {
		ProductDAO daoP=new ProductDAO();
	@Override
	public void validate(Product p) {
		Product p1=daoP.findById(p.getIdProduct());
		if(p1!=null){
			throw new IllegalArgumentException("A product with that id already exists");
		}
		
	}

}
