package bll;

import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.OrderDAO;
import dao.ProductDAO;
import model.Order;
import model.Product;

public class OrderBLL {
	
	
	private ArrayList<Product> order=new ArrayList();
	private static int pretTotal=0;
	public static String bon="";
	Order o;
	
	public OrderBLL(){
		
		
	}
	
	public void makeOrder(int idClient) throws SQLException{
		o=new Order();
		o.setClientId(idClient);
		OrderDAO.addOrder(o);
		System.out.println("Order created");
	}
	
	
public void addProduct(int idProduct,int quantity) throws SQLException{
		
		Product p=ProductDAO.findById(idProduct);
		
	
		if(quantity<=(-p.getStock())){
		order.add(p);
		pretTotal+=p.getPrice()*quantity;
		p.setStock(quantity);
		bon+=""+p.getName()+"........."+p.getPrice()+" lei x"+quantity+"\r\n";
		System.out.println(" Product added to order");
		OrderDAO.addToOrder(o, p);// update la baza de date in order si in join 
		ProductDAO.updateProduct(p, idProduct); // face update la produs pentru a schimab stocul
		}
		else
			System.out.println("\nNot enough "+p.getName()+" left \n We only have "+p.getStock());
	}

		public static int getPretTotal(){
			
			return pretTotal;
		}
		
		public static String getBon(){
			
			bon+="Pret total=  "+getPretTotal()+" lei";
		
			try {
			    PrintWriter writer = new PrintWriter("Bon.txt", "UTF-8");
			    writer.println(bon);
			    writer.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return bon;
		}

	
	
	public void finnishOrder(){
		o.getBon();
		System.out.println("Bon printat ");
		EmailSend.send();
	}

}
