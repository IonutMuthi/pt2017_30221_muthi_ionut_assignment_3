package bll;
import model.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import bll.validators.*;
import dao.*;
public class ClientBLL {
	
	private ArrayList<Validator<Client>> validators;

	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
		validators.add(new IdClientValidator());

	}

	public Client findClientById(int id) {
		Client st = ClientDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Client with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertClient(Client Client) throws SQLException {
		for (Validator<Client> v : validators) {
			v.validate(Client);
		}
		return ClientDAO.addClient(Client);
	}
	
	public void deleteClient(int id) throws SQLException{
		ClientDAO.deleteClient(id);
	}
	
	public void updateClient(Client c, int id) throws SQLException{
		ClientDAO.updateClient(c, id);
	}
	
	public ArrayList<Client> getClients() throws SQLException{
		return ClientDAO.getClients();
	}

}
