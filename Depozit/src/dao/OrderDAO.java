package dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.*;
import connection.*;

public class OrderDAO {
	
	public static Order findById(int orderId){
		Order toReturn=null;
		try{
			for(Order o:getOrders()){
				if(o.getIdOrder()==orderId)
					toReturn=new Order();
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return toReturn;
		
	}
	
	public static int addOrder(Order o) throws SQLException {
	Connection con = ConnectionFactory.getConnection();
	Statement s=con.createStatement();
	
	String order="INSERT INTO `warehouse_dataBase`.`order` (`idOrder`, `TotalPrice`, `Client_idClient`) VALUES( '"
			+o.getIdOrder()+3+"','"+o.getPretTotal()+"','"+o.getClientId()+"');";
		s.executeUpdate(order);
		
		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
		
	return o.getIdOrder();
	}
	
	public static void addToOrder(Order o,Product p) throws SQLException {
	Connection con = ConnectionFactory.getConnection();
	Statement s=con.createStatement();
	
	String order="INSERT INTO `warehouse_dataBase`.`order_has_product2` (`Order_idOrder`, `Product_idProduct`) VALUES( '"
			+o.getIdOrder()+"','"+p.getIdProduct()+"');";
		s.executeUpdate(order);
		
		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
		
	}
	
	public static ArrayList<Order> getOrders() throws SQLException{
		
		ArrayList<Order> orders=new ArrayList();
		Connection con=ConnectionFactory.getConnection();
		Statement stm=con.createStatement();
		ResultSet rs=stm.executeQuery("select * from order");
		while(rs.next()){
			Order o=new Order();
			//,Integer.parseInt(rs.getString("petTotal"))
			o.setIdOrder(Integer.parseInt(rs.getString("idOrder")));
			orders.add(o);
			System.out.println(o.getIdOrder()+" "+o.getPretTotal());		
		}

		ConnectionFactory.close(con);
		ConnectionFactory.close(stm);
		ConnectionFactory.close(rs);
		
		
		return orders;
	}

}
