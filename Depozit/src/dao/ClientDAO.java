package dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import model.*;
import connection.*;

public class ClientDAO {
	
	
	public static Client findById(int clientId) {
		Client toReturn = null;
			try {
				for(Client c:getClients()){
					if(c.getIdClient()==clientId)
						toReturn= new Client(c.getIdClient(),c.getName(),c.getAddress(),c.getAddress());
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return toReturn;
	}

	public static int addClient(Client c) throws SQLException {
		Connection con = ConnectionFactory.getConnection();
		Statement s=con.createStatement();
		System.out.println("aici");
	String client="INSERT INTO `warehouse_dataBase`.`client` (`idClient`, `Name`, `email`, `Address`) VALUES( '"
				+c.getIdClient()+"','"+c.getName()+"','"+c.getEmail()+"','"+c.getAddress()+"');";

			s.executeUpdate(client);
			
			ConnectionFactory.close(con);
			ConnectionFactory.close(s);
			
		return c.getIdClient();
	}
	
	public static void deleteClient(int idClient) throws SQLException{ // functie care sterge un client in baza de date 
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String client="DELETE FROM `warehouse_dataBase`.`client` WHERE `idClient`='"	+idClient+"';";
	
		s.executeUpdate(client);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
	}
	
	public static void updateClient(Client c,int id)throws SQLException{ // functie care face update la un client in baza de date 
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String client="UPDATE `warehouse_dataBase`.`client` SET `idClient`='"	+c.getIdClient()+"', `name`=' "+c.getName()
						+"' , `email`='"+c.getEmail()+"' , `address`='"+c.getAddress()+"'WHERE `idClient`='"+id+"';";
	
		s.executeUpdate(client);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
	}
	
	public static ArrayList<Client> getClients() throws SQLException{
		
		ArrayList<Client> clients=new ArrayList();
		Connection con = ConnectionFactory.getConnection();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from client");
		while(rs2.next()){
			Client c=new Client(Integer.parseInt(rs2.getString("idClient")),rs2.getString("name"),rs2.getString("address"),rs2.getString("email"));
			clients.add(c);
			System.out.println(c.getIdClient()+" " +c.getName()+" "+c.getEmail());		
		}

		ConnectionFactory.close(con);
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
		
		
		return clients;
	}
	
	

}
