package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.*;
import connection.*;

public class ProductDAO {
	
	public static Product findById(int productId) {
		Product toReturn = null;
			try {
				for(Product p:getProducts()){
					if(p.getIdProduct()==productId)
						toReturn= new Product(p.getName(),p.getPrice(),p.getStock(),p.getIdProduct());
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return toReturn;
	}
	
	

	public static int addProduct(Product p) throws SQLException{ // functie care adauga un client in baza de date 
		Connection con=ConnectionFactory.getConnection();;
		Statement s= con.createStatement();
		String product="INSERT INTO `warehouse_dataBase`.`product` (`idProduct`,`name`, `price`, `stock`) VALUES( '"
						+p.getIdProduct()+"','"+p.getName()+"','"+p.getPrice()+"','"+p.getStock()+"');";
		
		s.executeUpdate(product);
		
		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
		
				
		return p.getIdProduct();
	}
	
	public static void deleteProduct(int idProduct) throws SQLException{ // functie care sterge un client in baza de date 
		Connection con=ConnectionFactory.getConnection();;
		Statement s= con.createStatement();
		String product="DELETE FROM `warehouse_dataBase`.`product` WHERE `idProduct`='"	+idProduct+"';";
	
		s.executeUpdate(product);	
		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
		
	}
	
	public static void updateProduct(Product p,int idProduct)throws SQLException{ // functie care face update la un client in baza de date 
		Connection con=ConnectionFactory.getConnection();;
		Statement s= con.createStatement();
		String product="UPDATE `warehouse_dataBase`.`product` SET `name`='"	+p.getName()+"', `price`=' "+p.getPrice()
						+"' , `stock`='"+p.getStock()+"' ,`idProduct`='"+p.getIdProduct()+"'WHERE `idProduct`='"+idProduct+"';";
	
		s.executeUpdate(product);	
		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
		
	}
	
	public static ArrayList<Product> getProducts() throws SQLException{
		
		ArrayList<Product> products=new ArrayList();
		Connection con=ConnectionFactory.getConnection();;
		Statement stm=con.createStatement();
		ResultSet rs=stm.executeQuery("select * from product");
		while(rs.next()){
			Product p=new Product(rs.getString("name"),Integer.parseInt(rs.getString("price")),-(Integer.parseInt(rs.getString("stock"))),Integer.parseInt(rs.getString("idProduct")));
			products.add(p);	
			System.out.println(p.getIdProduct()+" "+p.getName()+" "+p.getPrice()+" "+p.getStock());		
		}
		
		ConnectionFactory.close(con);
		ConnectionFactory.close(stm);
		ConnectionFactory.close(rs);
		
		return products;
	}

}
